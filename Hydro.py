#!/usr/bin/env python3

import wx.adv
import wx
import vlc
import time
import subprocess
import threading
from threading import Thread
TRAY_TOOLTIP = 'Hydro'
TRAY_ICON = '/home/warmerspy/PycharmProjects/hydro/dist/resources/glass.png'
sound_file = vlc.MediaPlayer("/home/warmerspy/PycharmProjects/hydro/dist/resources/pouring_water.mp3")
notiinterval = 10


def create_menu_item(menu, label, func):
    item = wx.MenuItem(menu, -1, label)
    menu.Bind(wx.EVT_MENU, func, id=item.GetId())
    menu.Append(item)
    return item


class TaskBarIcon(wx.adv.TaskBarIcon):
    def __init__(self, frame):
        self.frame = frame
        super(TaskBarIcon, self).__init__()
        self.set_icon(TRAY_ICON)
        self.Bind(wx.adv.EVT_TASKBAR_LEFT_DOWN, self.on_left_down)

    def CreatePopupMenu(self):
        menu = wx.Menu()
        create_menu_item(menu, 'Start', self.on_start)
        menu.AppendSeparator()
        create_menu_item(menu, 'Exit', self.on_exit)
        return menu

    def set_icon(self, path):
        icon = wx.Icon(path)
        self.SetIcon(icon, TRAY_TOOLTIP)

    def on_left_down(self, event):
        print ('Tray icon was left-clicked.')

    def on_start(self, event):
        Thread(target=notification).start()
        Thread.daemon = True

    def on_exit(self, event):
        wx.CallAfter(self.Destroy)
        self.frame.Close()


def notification():
    subprocess.run(
        'notify-send "Hydro is now running!"', shell=True, check=True)
    while True:
        time.sleep(notiinterval)
        subprocess.run(
            'notify-send "Make sure you stay hydrated. Right now you should at least have consumed a glass of '
            'water or any other type of beverage"', shell=True, check=True)
        sound_file.play()
        time.sleep(6)
        sound_file.stop()


class App(wx.App):
    def OnInit(self):
        frame=wx.Frame(None)
        self.SetTopWindow(frame)
        TaskBarIcon(frame)
        return True


def main():
    app = App(False)
    app.MainLoop()


if __name__ == '__main__':
    main()

